package movies.tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.ImdbImporter;

class ImdbImporterTests {

	private ImdbImporter imdb = new ImdbImporter("src\\texts\\imdb", "src\\out\\imdbKaggleExport");
	
	@Test
	void testRegularLine() 
	{
		ArrayList<String> testArrayList = new ArrayList<>();
		testArrayList.add("tt0003471	Traffic in Souls	Traffic in Souls	1913	11/24/1913	\"Crime, Drama\"	88	USA	English	George Loane Tucker		Independent Moving Pictures Co. of America (IMP)	\"Jane Gail, Ethel Grandin, William H. Turner, Matt Moore, William Welsh, Millie Liston, Irene Wallace, William Cavanaugh, Arthur Hunter, Howard Crampton, W.H. Bainbridge, Luray Huntley, William Powers, Jack Poulton, Edward Boring\"	\"A woman, with the aid of her police officer sweetheart, endeavors to uncover the prostitution ring that has kidnapped her sister, and the philanthropist who secretly runs it.\"	6	552	\"$5,700 \"				14	11");
		
		String expected = "Traffic in Souls	1913\t88\tIMBD";
		
		assertEquals(expected, imdb.process(testArrayList).get(0));
	}
	
	@Test
	void testShorterLine() 
	{
		ArrayList<String> testArrayList = new ArrayList<>();
		testArrayList.add("tt0003471	Traffic in Souls	Traffic in Souls	1913	11/24/1913	\"Crime, Drama\"	88	USA	English	George Loane Tucker		Independent Moving Pictures Co. of America (IMP)	\"Jane Gail, Ethel Grandin, William H. Turner, Matt Moore, William Welsh, Millie Liston, Irene Wallace, William Cavanaugh, Arthur Hunter, Howard Crampton, W.H. Bainbridge, Luray Huntley, William Powers, Jack Poulton, Edward Boring\"	\"A woman, with the aid of her police officer sweetheart, endeavors to uncover the prostitution ring that has kidnapped her sister, and the philanthropist who secretly runs it.\"	6	552	\"$5,700 \"				14");

		String expected = "Traffic in Souls	1913\t88\tIMBD";
		
		assertEquals(expected, imdb.process(testArrayList).get(0));
	}
}
