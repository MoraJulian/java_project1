package movies.tests;
import movies.importer.*;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

/**
 * @author Etienne Plante & Julian Mora
 *
 */
class MovieTests {
	
	@Test
	void testGetNameMethod() {
		Movie testMovie = new Movie("ParmesanCheese","2020","120","Jihad");
		assertEquals("ParmesanCheese",testMovie.getName());
	}
	
	@Test
	void testGetReleaseYearMethod() {
		Movie testMovie = new Movie("ParmesanCheese","2020","120","Jihad");
		assertEquals("2020",testMovie.getReleaseYear());
	}
	
	@Test
	void testGetRuntime() {
		Movie testMovie = new Movie("ParmesanCheese","2020","120","Jihad");
		assertEquals("120",testMovie.getRuntime());
	}
	
	@Test
	void testGetSource() {
		Movie testMovie = new Movie("ParmesanCheese","2020","120","Jihad");
		assertEquals("Jihad",testMovie.getSource());
	}
	
	@Test
	void testSetNameMethod() {
		Movie testMovie = new Movie("ParmesanCheese","2020","120","Jihad");
		testMovie.setName("ABigPenny");
		assertEquals("ABigPenny",testMovie.getName());
	}
	
	@Test
	void testSetReleaseYearMethod() {
		Movie testMovie = new Movie("ParmesanCheese","2020","120","Jihad");
		testMovie.setReleaseYear("1999");
		assertEquals("1999",testMovie.getReleaseYear());
	}

	@Test
	void testSetRuntime() {
		Movie testMovie = new Movie("ParmesanCheese","2020","120","Jihad");
		testMovie.setRuntime("150");
		assertEquals("150",testMovie.getRuntime());
	}

	@Test
	void testSetSource() {
		Movie testMovie = new Movie("ParmesanCheese","2020","120","Jihad");
		testMovie.setSource("CindyFromJimmyNeutron");
		assertEquals("CindyFromJimmyNeutron",testMovie.getSource());
	}
	
	@Test
	void testGetToString() {
		Movie testMovie = new Movie("ParmesanCheese","2020","120","Jihad");
		assertEquals("ParmesanCheese	2020	120	Jihad",testMovie.toString());
	}
	
	@Test
	void testEqualsIsEqual() {
		Movie movie1 = new Movie("ParmesanCheese","2020","120","Jihad");
		Movie movie2 = new Movie("ParmesanCheese","2020","120","Jihad");
		assertTrue(movie1.equals(movie2));
	}
	
	@Test
	void testEqualsNameDifferent() {
		Movie movie1 = new Movie("ParmesanCheese","2020","120","Jihad");
		Movie movie2 = new Movie("GoudaCheese","2020","120","Jihad");
		assertFalse(movie1.equals(movie2));
	}
	
	@Test
	void testEqualsYearDifferent() {
		Movie movie1 = new Movie("ParmesanCheese","2020","120","Jihad");
		Movie movie2 = new Movie("ParmesanCheese","2010","120","Jihad");
		assertFalse(movie1.equals(movie2));
	}
	
	@Test
	void testEqualsDurationDifferent() {
		Movie movie1 = new Movie("ParmesanCheese","2020","120","Jihad");
		Movie movie2 = new Movie("ParmesanCheese","2020","90","Jihad");
		assertFalse(movie1.equals(movie2));
	}
}
