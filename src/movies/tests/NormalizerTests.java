package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import movies.importer.Normalizer;


class NormalizerTests {

	Normalizer normalize = new Normalizer(" "," ");
	
	@Test
	void testProcess() throws IOException {
		
		ArrayList<String> test = new ArrayList<String>();
		test.add("The Mummy: Tomb of the Dragon Emperor	2008	112 minutes	kaggle");
		
		assertEquals("the mummy: tomb of the dragon emperor	2008	112	kaggle",normalize.process(test).get(0));
	}
	
	@Test
	void testRemoveMinuteString() throws IOException {
		
		String[] test = {"the mummy: tomb of the dragon emperor"," 	2008","112 minutes","kaggle" };
		assertEquals("112",normalize.removeMinutesString(test));
	}
	
	@Test
	void testAddUntoValidatedArray() throws IOException {
		
		ArrayList<String> arrL = new ArrayList<String>();
		String[] strArr = {"the mummy: tomb of the dragon emperor"," 	2008","112 minutes","kaggle"};
		normalize.addUntoValidatedArray(strArr, arrL);
		
		assertEquals("the mummy: tomb of the dragon emperor	 	2008	112 minutes	kaggle",arrL.get(0));
	}
	

}
