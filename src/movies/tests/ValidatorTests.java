package movies.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Movie;
import movies.importer.Validator;

class ValidatorTests {

	private Validator validator = new Validator("src\\out\\imdbKaggleExport", "src\\out\\validatorExport");
	
	///PROCESS
	@Test
	void testAllOk() 
	{
		ArrayList<String> testArrayList = new ArrayList<>();
		testArrayList.add("Miss Jerry\t1894\t45\tIMDB");
		
		assertEquals(1, validator.process(testArrayList).size());
	}

	@Test
	void testMissingSource() 
	{
		ArrayList<String> testArrayList = new ArrayList<>();
		testArrayList.add("Miss Jerry\t1894\t45");
		testArrayList.add("Miss Jerry\t1894\t45\t");
		
		assertEquals(0, validator.process(testArrayList).size());
	}
	
	@Test
	void testMissingRuntime() 
	{
		ArrayList<String> testArrayList = new ArrayList<>();
		testArrayList.add("Miss Jerry\t1894\t\tIMDB");
		
		assertEquals(0, validator.process(testArrayList).size());
	}
	
	@Test
	void testMissingYear() 
	{
		ArrayList<String> testArrayList = new ArrayList<>();
		testArrayList.add("Miss Jerry\t\t45\tIMDB");
		
		assertEquals(0, validator.process(testArrayList).size());
	}
	
	@Test
	void testMissingTitle() 
	{
		ArrayList<String> testArrayList = new ArrayList<>();
		testArrayList.add("\t1894\t45\tIMDB");
		
		assertEquals(0, validator.process(testArrayList).size());
	}
	
	@Test
	void testReleaseNotNumber() 
	{
		ArrayList<String> testArrayList = new ArrayList<>();
		testArrayList.add("Miss Jerry\tNaN\t45\tIMDB");
		
		assertEquals(0, validator.process(testArrayList).size());
	}
	
	///INDIVIDUAL METHODS
	@Test
	void testStringToMovie()
	{
		Movie expected = new Movie("ParmesanCheese","2020","120","Jihad");
		String movieString = "ParmesanCheese\t2020\t120\tJihad";
		
		assertEquals(expected.getName(), validator.stringToMovie(movieString).getName());
		assertEquals(expected.getReleaseYear(), validator.stringToMovie(movieString).getReleaseYear());
		assertEquals(expected.getRuntime(), validator.stringToMovie(movieString).getRuntime());
		assertEquals(expected.getSource(), validator.stringToMovie(movieString).getSource());
	}
	
	@Test
	void testAllEntriesFilled()
	{
		Movie testMovie = new Movie("ParmesanCheese","2020","120","Jihad");
		assertTrue(validator.checkAllEntriesFilled(testMovie));
	}
	
	@Test
	void testAllEntriesNotFilledEmpty()
	{
		Movie testMovie = new Movie("","2020","120","JIhad");
		assertFalse(validator.checkAllEntriesFilled(testMovie));
	}
	
	@Test
	void testAllEntriesNotFilledNull()
	{
		Movie testMovie = new Movie(null,"2020","120", "Jihad");
		assertFalse(validator.checkAllEntriesFilled(testMovie));
	}
	
	@Test
	void testReleaseRuntimeAreNumber()
	{
		Movie testMovie = new Movie("ParmesanCheese" ,"2020","120", "Jihad");
		assertTrue(validator.releaseAndRuntimeAreNumbers(testMovie));
	}
	
	@Test
	void testReleaseRuntimeNotNumber()
	{
		Movie testMovie = new Movie("ParmesanCheese" ,"2020t","12s0", "Jihad");
		assertFalse(validator.releaseAndRuntimeAreNumbers(testMovie));
	}
}
