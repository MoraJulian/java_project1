package movies.importer;

/**
 * @author Julian Mora
 *	A class that represents a single movie and its details
 */
public class Movie {
	//Name of the movie
	private String name;
	
	//The year the movie released 
	private String releaseYear;
	
	//The duration of the movie
	private String runtime;
	
	//Where the movie came from
	private String source;
	
	

	/**
	 * @param V_name
	 * @param V_releaseYear
	 * @param V_runtime
	 * @param V_source
	 */
	public Movie(String V_name, String V_releaseYear, String V_runtime, String V_source) {
		this.name = V_name;
		this.releaseYear = V_releaseYear;
		this.runtime = V_runtime;
		this.source = V_source;
	}
	
	/**
	 * @return the release year of the Movie
	 */
	public String getReleaseYear() {
		return releaseYear;
	}
	
	
	/**
	 * @param releaseYear the releaseYear to set
	 */
	public void setReleaseYear(String releaseYear) {
		this.releaseYear = releaseYear;
	}
	
	
	/**
	 * @return the name of the Movie
	 */
	public String getName() {
		return name;
	}
	
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	/**
	 * @return the runtime of the Movie
	 */
	public String getRuntime() {
		return runtime;
	}
	
	
	/**
	 * @param runtime the runtime to set
	 */
	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}
	
	
	/**
	 * @return the source where the Movie came from
	 */
	public String getSource() {
		return source;
	}
	
	
	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}
	
	
	/**
	 * @return returns a string representation of the Movie Object (separated by tabs)
	 */
	public String toString() {
		return this.name+"\t"+this.releaseYear+"\t"+this.runtime+"\t"+this.source;
		
	}
	
	
	/**Override of .equals() method that checks if the name/releaseYear/duration of the movies 
	 * compared are the same.
	 */
	@Override
	public boolean equals(Object o){
		
		Movie m = (Movie) o;
		
		int runtimeDifference = Math.abs(Integer.parseInt(this.runtime) - Integer.parseInt(m.getRuntime()));
		
		if(m.getName().toLowerCase().equals(name.toLowerCase()) && m.getReleaseYear().equals(this.releaseYear) && runtimeDifference <= 5) {
			return true;
		}
		
		return false;	
	}
	
	
	
	
}
