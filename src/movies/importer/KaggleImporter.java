package movies.importer;

import java.util.ArrayList;

/**A Class that extends the Processor Class that provides a format from the Movie list that respects the Movie Class
 * @author Julian Mora
 *
 */
public class KaggleImporter extends Processor {
	
	/**
	 * @param sourceDir Source directory for RottenTomatoes list
	 * @param outputDir Output directory for processed RottenTomatoes list
	 */
	public KaggleImporter(String sourceDir, String outputDir) {
		super(sourceDir,outputDir,true);
	}
	

	
	/**Incrementally adds a String representation of each Movie unto a new processed ArrayList
	 *@return A processed ArrayList
	 */
	public ArrayList<String> process(ArrayList<String> inputArray){
		ArrayList<String> processedKaggleArray = new ArrayList<String>();
		
		//Loops through the inputArray and formats each of its elements
		for(int i =0; i<inputArray.size();i++) {
			//Creates a String array of the Movie Object
			String[] splicedLine = inputArray.get(i).split("\\t",-1);
			
			
			//Creates the formated Movie Object and adds it to the processed ArrayList
			Movie lineMovie = new Movie(splicedLine[15],splicedLine[20],splicedLine[13],"kaggle");
			processedKaggleArray.add(lineMovie.toString());
			
			
		}
		
		return processedKaggleArray;
 	}
}
