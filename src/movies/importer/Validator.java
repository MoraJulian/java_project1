package movies.importer;

import java.util.ArrayList;

/**Validates an array of movies removing any invalid entry
 * @author Etienne Plante
 */
public class Validator extends Processor {
	/**
	 * @param sourceDir Source directory for list
	 * @param outputDir Output directory for processed list
	 */
	public Validator(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}

	/**Processes the string list from the IMDBImporter and returns a new list containing only the valid entries
	 * @param input The lines from the file
	 * @return The processed movies
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> validatedList = new ArrayList<String>();
		
		for(String movieStr : input)
		{
			Movie movie = stringToMovie(movieStr);
			
			if(checkAllEntriesFilled(movie) && releaseAndRuntimeAreNumbers(movie)) { validatedList.add(movie.toString()); }
		}
		
		return validatedList;
	}
	
	/**Turns a string in a movie object
	 * @param input The movie string
	 * @return The movie object
	 */
	public Movie stringToMovie(String str)
	{
		String[] strFields = str.split("\\t");
		
		//If the required values don't exist, assign null
		String name = strFields.length >= 1 ? strFields[0] : null;
		String releaseYear = strFields.length >= 2 ? strFields[1] : null;
		String runtime = strFields.length >= 3 ? strFields[2] : null;
		String source = strFields.length >= 4 ? strFields[3] : null;

		
		return new Movie(name, releaseYear, runtime, source);
	}
	
	/**Checks if all entries in a movie are neither empty strings nor null
	 * @param input The movie object
	 * @return Whether or not all entries are filled
	 */
	public boolean checkAllEntriesFilled(Movie movie)
	{
		boolean titlePresent =  movie.getName() != null && !movie.getName().trim().isEmpty();
		boolean releasePresent = movie.getReleaseYear() != null && !movie.getReleaseYear().trim().isEmpty();
		boolean runtimePresent = movie.getRuntime() != null && !movie.getRuntime().trim().isEmpty();
		boolean sourcePresent = movie.getSource() != null && !movie.getSource().trim().isEmpty();
		
		return titlePresent && releasePresent && runtimePresent && sourcePresent;
	}
	
	/**Checks that the runtime and release year of a movie are both numbers
	 * @param input The movie object
	 * @return Whether or not all entries are filled
	 */
	public boolean releaseAndRuntimeAreNumbers(Movie movie)
	{
		try
		{
			Integer.parseInt(movie.getReleaseYear());
			Integer.parseInt(movie.getRuntime());
		} catch (NumberFormatException e) {
			return false;
		}
		
		return true;
	}
}