package movies.importer;

import java.util.ArrayList;

/**A Class that extends the Processor Class and normalized a previously formatted(from KaggleImporter) Movie ArrayList 
 * @author Julian Mora
 *
 */
public class Normalizer extends Processor {

	/**
	 * @param sourceDir
	 * @param outputDir
	 */
	public Normalizer(String sourceDir, String outputDir) {
		super(sourceDir,outputDir,false);
	}
	
	
	/**Incrementally adds the normalized String representation of a Movie into a new normalized ArrayList
	 *@return A normalized ArrayList
	 */
	public ArrayList<String> process(ArrayList<String> inputArray){
		ArrayList<String> validatedArray = new ArrayList<String>();
		
		//Loops through the inputArray and normalizes each of its elements
		for(int i=0;i<inputArray.size();i++) {
			//Creates a string array of the Movie Object, then replaces the title with a LowerCase title
			String[] movieObjectRepresentation = inputArray.get(i).split("\\t");
			movieObjectRepresentation[0] = movieObjectRepresentation[0].toLowerCase();
			
			
			//Replaces the runtime in the movieObjectArray to only the first "word" (digits)
			movieObjectRepresentation[2] = removeMinutesString(movieObjectRepresentation);
			
			//Here i add the normalized Movie to the validatedArrayList
			addUntoValidatedArray(movieObjectRepresentation,validatedArray);
		}
		
		return validatedArray;
	}
	
	
	/**Extracts the digit portion of the releaseYear
	 * @param movieObjectRepresentation
	 * @return Digit release year
	 */
	public String removeMinutesString(String[] movieObjectRepresentation) {
		String[] runtimeArray = movieObjectRepresentation[2].trim().split("\\s+"); 
		return runtimeArray[0]; 
	}

	
	/**Adds the normalized version of the Movie object into the validated ArrayList
	 * @param normalizedMovieObject
	 * @param validatedArray
	 */
	public void addUntoValidatedArray(String[] normalizedMovieObject,ArrayList<String> validatedArray) {
		Movie normalizedMovie = new Movie(normalizedMovieObject[0],normalizedMovieObject[1],normalizedMovieObject[2],normalizedMovieObject[3]);
		validatedArray.add(normalizedMovie.toString());
	}
	
}


