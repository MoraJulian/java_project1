package movies.importer;

import java.util.ArrayList;

/**A class extending Processor that standardizing the format of text files
 * @author Etienne Plante
 */
public class ImdbImporter extends Processor {

	/**
	 * @param sourceDir Source directory for imbd list
	 * @param outputDir Output directory for processed imdb list
	 */
	public ImdbImporter(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}

	/**Processes the string array from an IMDB file into an array list of string representations of movies
	 * @param input The lines from the file
	 * @return The processed movies
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> processed = new ArrayList<String>();
		
		for(String s : input)
		{		
			String[] splitValues = s.split("\\t", -1);
			
			Movie formattedMovie = stringArrayToMovie(splitValues);
			
			processed.add(formattedMovie.toString());
		}
		
		return processed;
	}
	
	/**Turns a string in a movie object
	 * @param input The movie string
	 * @return The movie object
	 */
	public Movie stringArrayToMovie(String[] strArr)
	{
		String name = strArr[1];
		String releaseYear = strArr[3];
		String runtime = strArr[6];
		
		return new Movie(name, releaseYear, runtime, "IMBD");
	}
}
