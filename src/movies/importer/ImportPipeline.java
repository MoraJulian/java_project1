package movies.importer;

import java.io.IOException;

/**
 * @author Etienne Plante
 *	 The starting point of the application
 */
public class ImportPipeline {
	public static void main(String[] args) throws IOException 
	{
		Processor[] processors = new Processor[5];
		processors[0] = new ImdbImporter("src\\texts\\imdb", "src\\out\\imdbKaggleExport");
		processors[1] = new KaggleImporter("src\\texts\\kaggle", "src\\out\\imdbKaggleExport");
		processors[2] = new Normalizer("src\\out\\imdbKaggleExport", "src\\out\\normalizerExport");
		processors[3] = new Validator("src\\out\\normalizerExport", "src\\out\\validatorExport");
		processors[4] = new Deduper("src\\out\\validatorExport", "src\\out\\deduperExport");
		
		processAll(processors);
	}
	
	/**Executes all the processors
	 * @param processors The processors array
	 */
	public static void processAll(Processor[] processors) throws IOException
	{
		for(Processor proc : processors)
		{
			proc.execute();
		}
	}
}
