package movies.importer;

import java.util.ArrayList;

/** A Class that extends the Processor Class and creates a new ArrayList with no duplicates
 * @author Julian Mora
 *
 */
public class Deduper extends Processor{
	
	/**
	 * @param mergedArray
	 * @param sourceDir
	 * @param outputDir
	 */
	public Deduper( String sourceDir, String outputDir) {
		super(sourceDir,outputDir,false);
	}
	
	
	/** Creates a new ArrayList of Movie from input String ArrayList and dedupes it
	 *
	 */
	public ArrayList<String> process(ArrayList<String> inputArray){
		
		//Declaring/Initializing both Movie ArrayLists & Movie object
		ArrayList<Movie> movieList = toMovieArrayList(inputArray);
		ArrayList<Movie> noDuplicateMovieList = new ArrayList<Movie>();
		Movie currentMovie;

		/* Loops through the duplicate Movie Array to then check whether or not the noDuplicate Array
		 * contains the currentMovie. If it does not contain it, add unto the noDuplicate array, if it
		 * does contain it, merge it into one movie, THEN replace it in noDuplicate Array. 
		*/
		for(int i =0; i < movieList.size();i++) {
			currentMovie = movieList.get(i);
			
			if(!noDuplicateMovieList.contains(currentMovie)) {
				noDuplicateMovieList.add(currentMovie);
			}
				
			else {
				//Store index of the duplicate and the new shared source "IMDB;Kaggle" or "Kaggle;IMDB"
				//As well as storing the two movie sources 
				int indexOfDuplicate = noDuplicateMovieList.indexOf(currentMovie);
				String currentMovieSource = currentMovie.getSource();
				String duplicateSource = noDuplicateMovieList.get(indexOfDuplicate).getSource();
				
				//If validation (In case duplicate comes from same text file  e.g. "IMDB;IMDB")
				if(!currentMovieSource.equals(duplicateSource)) {
					String newSource = duplicateSource + ";" + currentMovieSource;
			
					//Inserts newly created non duplicated Movie into noDuplicateMovieList 
					Movie dedupedMovie = new Movie(currentMovie.getName(),currentMovie.getReleaseYear(),currentMovie.getRuntime(),newSource);
					noDuplicateMovieList.set(indexOfDuplicate, dedupedMovie);
				}
			}
		}	
		
		//Convert Movie ArrayList to String ArrayList and returns it
		return toStringArrayList(noDuplicateMovieList);
	}
	
	
	/** Takes the initial mergedArray and transforms it into a Movie ArrayList
	 * @param inputArray
	 * @return
	 */
	public ArrayList<Movie> toMovieArrayList(ArrayList<String> inputArray){
		ArrayList<Movie> movieList = new ArrayList<Movie>();
		
		for(int i = 0; i < inputArray.size(); i++) {
			String[] splicedMovie = inputArray.get(i).split("\\t");
			Movie lineMovie = new Movie(splicedMovie[0],splicedMovie[1],splicedMovie[2],splicedMovie[3]);
			movieList.add(lineMovie);
		}
		return movieList;
	}
	
	/**Takes the Movie ArrayList and converts it to a String ArrayList
	 * @param inputArray
	 * @return
	 */
	public ArrayList<String> toStringArrayList(ArrayList<Movie> inputArray){
		ArrayList<String> dedupedArray = new ArrayList<String>();
		for(int i = 0; i < inputArray.size(); i++) {
			dedupedArray.add(inputArray.get(i).toString());
		}
		return dedupedArray;
	}
	
	
}
